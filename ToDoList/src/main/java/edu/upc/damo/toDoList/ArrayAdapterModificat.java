package edu.upc.damo.toDoList;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.List;


/**
 * Created by Josep M on 16/09/2015.
 */

public class ArrayAdapterModificat<CharSequence> extends ArrayAdapter<CharSequence> {
    List<CharSequence> dades;

    public ArrayAdapterModificat(Context context, int resource) {
        super(context, resource);
    }

    public ArrayAdapterModificat(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public ArrayAdapterModificat(Context context, int resource, CharSequence[] objects) {
        super(context, resource, objects);
    }

    public ArrayAdapterModificat(Context context, int resource, int textViewResourceId, CharSequence[] objects) {
        super(context, resource, textViewResourceId, objects);
    }



    public ArrayAdapterModificat(Context context, int resource, int textViewResourceId, List<CharSequence> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public ArrayAdapterModificat(Context context, int resource, List<CharSequence> objects) {
        super(context, resource, objects);
        dades = objects;
    }


    public void remove(int position){
        dades.remove(position);
        notifyDataSetChanged();
    }
}
