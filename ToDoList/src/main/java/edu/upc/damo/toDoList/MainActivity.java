package edu.upc.damo.toDoList;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends Activity {
    private Boolean b=true;
    private EditText nouText;
    private ListView listView;
    private List<CharSequence> dades =new ArrayList<CharSequence>();

   // ArrayAdapter<CharSequence> adaptador;

   ArrayAdapterModificat<CharSequence> adaptador;

    private Teclat teclat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.llista);

        carregaDades();
        inicialitza();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void carregaDades(){
        CharSequence [] dadesEstatiques = getResources().getStringArray(R.array.dadesEstatiques);
        Collections.addAll(dades, dadesEstatiques);
    }


    private void inicialitza(){

        nouText = (EditText) findViewById(R.id.nouText);
        listView = (ListView) findViewById(R.id.llista);
        adaptador = new ArrayAdapterModificat(this,android.R.layout.simple_list_item_1,dades);
        listView.setAdapter(adaptador);

        teclat = new Teclat(this);


        nouText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                switch (teclat.accio(actionId, keyEvent)) {
                    case Teclat.OK:
                        nouText(textView.getText().toString());
                        textView.setText("");
                        return true;
                }

                return true;
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                treuText(position);


                return false;  //????????????????
            }
        });

    }

    private void treuText(int position) {
        adaptador.remove(position);

        // ((ArrayAdapterModificat<CharSequence>) adaptador).remove(position);
    }

    private void nouText(CharSequence text) {
        adaptador.add(text);
    }
}
